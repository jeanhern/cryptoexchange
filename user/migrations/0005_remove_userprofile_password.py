# Generated by Django 2.2.6 on 2019-10-12 20:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0004_userprofile_username'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='password',
        ),
    ]
