from django.urls import path, re_path, include
from rest_framework import routers
from .views import CreateBalanceView, BalanceView


urlpatterns = [
    path('create', CreateBalanceView.as_view(), name='createBalance'),
    path('get/<str:username>/', BalanceView.as_view(), name='getBalance'),

]
