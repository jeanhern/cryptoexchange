from rest_framework.views import APIView
from rest_framework.response import Response
from user.models import UserProfile
from .models import Balance, Transfer, Offer, Trade
from currency.models import Currency
from .serializer import TransferSerializer, OfferSerializer, TradeSerializer
from itertools import chain


def userdoesnotexist(user):
    return {"error": "404 user " + user + " does not exist"}


def balancedoesnotexist():
    return {"error": "404 balance does not exist"}


def offerdoesnotexist(user):
    return {"error": "404 user " + user + " does not have standing offers"}


class CreateOfferView(APIView):

    def post(self, request):
        body = request.POST or request.data
        try:
            balance = Balance.objects.get(user__username=body["user"], currency__symbol=body["offer_currency"])
            if float(balance.balance) - float(balance.blocked_balance) >= float(body["amount"]):
                balance.blocked_balance = float(balance.blocked_balance) + float(body["amount"])
                balance.save()
                offer = Offer(
                    amount=float(body["amount"]),
                    offer_currency=Currency.objects.get(symbol=body["offer_currency"]),
                    price=float(body["price"]),
                    wanted_currency=Currency.objects.get(symbol=body["wanted_currency"]),
                    user=UserProfile.objects.get(username=body["user"])
                )
                offer.save()
                return Response(OfferSerializer(offer).data)
            else:
                return Response({"error": "Balance is insufficient to perform the requested offer"})
        except Balance.DoesNotExist:
            return Response(balancedoesnotexist())


class OfferView(APIView):

    def get(self, request, username):
        try:
            offers = Offer.objects.filter(user__username=username)
            return Response(OfferSerializer(offers, many=True).data)
        except Offer.DoesNotExist:
            return Response(offerdoesnotexist(username))


class OfferByCurrencyView(APIView):
    def get(self, request, offer_currency, want_currency, username):
        try:
            print('hola')
            offers = Offer.objects.filter(
                offer_currency__symbol=offer_currency,
                want_currency__symbol=want_currency
            )
            print(offers)
            if username:
                try:
                    offers.filter(user__username=username)
                except Offer.DoesNotExist:
                    return Response({"error": " 404 user " + username + " doesn't have an standing offer for the given currencies"})
            return Response(OfferSerializer(offers, many=True).data)
        except Offer.DoesNotExist:
            return Response({"error": "404 there are no offers for the given currencies"})

    def post(self, request):
        body = request.POST or request.data
        try:
            offers = Offer.objects.filter(
                offer_currency__symbol=body["offer_currency"],
                want_currency__symbol=body["want_currency"]
            )
            print(offers)
            if body["username"]:
                try:
                    offers.filter(user__username=body["username"])
                    return Response(OfferSerializer(offers, many=True).data)
                except Offer.DoesNotExist:
                    return Response({"error": " 404 user " + body["username"] + " doesn't have an standing offer for the given currencies"})
        except Offer.DoesNotExist:
            return Response({"error": "404 there are no offers for the given currencies"})


class TransferView(APIView):
    def get(self, request, username):
        try:
            received_transfers = Transfer.objects.filter(to_balance__user__username=username)
        except Transfer.DoesNotExist:
            received_transfers = []
        try:
            sent_transfers = Transfer.objects.filter(from_balance__user__username=username)
        except Transfer.DoesNotExist:
            sent_transfers = []
        return Response({
                "received": TransferSerializer(received_transfers, many=True).data,
                "sent": TransferSerializer(sent_transfers, many=True).data
            })
