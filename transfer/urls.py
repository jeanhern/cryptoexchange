from django.urls import path, re_path, include
from rest_framework import routers
from .views import CreateOfferView, OfferView, OfferByCurrencyView, TransferView


urlpatterns = [
    path('offer/create', CreateOfferView.as_view(), name='createOffer'),
    path('offer/<str:username>/', OfferView.as_view(), name='getOffers'),
    path(r'^offer/bycurrency/$', OfferByCurrencyView.as_view(), name='getOfferByCurrency'),
    path('get/<str:username>/', TransferView.as_view(), name='getTransfers')
]
