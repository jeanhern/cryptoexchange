from .models import Offer, Transfer, Trade
from rest_framework.serializers import ModelSerializer


class OfferSerializer(ModelSerializer):
    class Meta:
        model = Offer
        fields = ('id',
                  'amount',
                  'offer_currency',
                  'price',
                  'wanted_currency',
                  'status',
                  'datetime',
                  'user'
                  )


class TransferSerializer(ModelSerializer):
    class Meta:
        model = Transfer
        fields = ('from_balance',
                  'to_balance',
                  'amount',
                  'currency',
                  'datetime'
                  )


class TradeSerializer(ModelSerializer):
    class Meta:
        model = Trade
        fields = ('initial_transfer', 'final_transfer')
