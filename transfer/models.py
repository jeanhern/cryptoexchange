from django.db import models
from currency.models import Currency
from user.models import UserProfile
from django.utils import timezone
from balance.models import Balance

STATUS = (('wait', 'Waiting in line',),
          ('complete', 'Offer has been completed'))


class Offer(models.Model):
    user = models.ForeignKey(UserProfile, related_name='user_offer', on_delete=None)
    amount = models.DecimalField(max_digits=8, decimal_places=6)
    offer_currency = models.ForeignKey(Currency, related_name='offer_currency', on_delete=None)
    price = models.DecimalField(max_digits=20, decimal_places=6)
    wanted_currency = models.ForeignKey(Currency, related_name='wanted_currency', on_delete=None)
    status = models.CharField(max_length=8, choices=STATUS, default='wait')
    datetime = models.DateTimeField(default=timezone.now)


class Transfer(models.Model):
    from_balance = models.ForeignKey(Balance, related_name='send_balance', on_delete=None)
    to_balance = models.ForeignKey(Balance, related_name='receive_balance', on_delete=None)
    amount = models.DecimalField(max_digits=8, decimal_places=6)
    currency = models.ForeignKey(Currency, related_name='transfer_currency', on_delete=None)
    datetime = models.DateTimeField(default=timezone.now)


class Trade(models.Model):
    initial_transfer = models.ForeignKey(Transfer, related_name='related_exchange', on_delete=None)
    final_transfer = models.ForeignKey(Transfer, related_name='related_exchange_2', on_delete=None)