from rest_framework.views import APIView
from rest_framework.response import Response
from user.models import UserProfile
from .models import Balance
from currency.models import Currency
from .serializer import BalanceSerializer


def userdoesnotexist(user):
    return {"error": "404 user " + user + " does not exist"}


def balancedoesnotexist():
    return {"error": "404 balance does not exist"}


def offerdoesnotexist(user):
    return {"error": "404 user " + user + " does not have standing offers"}


class CreateBalanceView(APIView):

    def post(self, request):
        body = request.data or request.POST
        try:
            currency = Currency.objects.get(symbol=body["currency"])
            try:
                user = UserProfile.objects.get(username=body["user"])
            except UserProfile.DoesNotExist:
                return Response(userdoesnotexist(body["user"]))
            balance = Balance.objects.filter(currency=currency, user=user).first()
            if balance:
                balance.balance = float(balance.balance) + float(body["balance"])
            else:
                balance = Balance(
                    balance=body["balance"],
                    blocked_balance=0.0,
                    user=user,
                    currency=currency
                )
            balance.save()
            return Response(BalanceSerializer(balance).data)

        except Currency.DoesNotExist:
            return Response({"error": "ERROR 404 currency not found"})


class BalanceView(APIView):

    def get(self, request, username):
        try:
            user = UserProfile.objects.get(username=username)
            try:
                balances = Balance.objects.filter(user=user)
                return Response(BalanceSerializer(balances, many=True).data)
            except Balance.DoesNotExist:
                return Response(balancedoesnotexist())
        except UserProfile.DoesNotExist:
            return Response(userdoesnotexist(username))
