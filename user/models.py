from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    userid = models.OneToOneField(User, on_delete=None)
    username = models.CharField(max_length=20, unique=True, null=True)
    location = models.CharField(max_length=40, blank=True)

    def __str__(self):
        return self.username
